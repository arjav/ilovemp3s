playlist={}
currentSong=1
loaded=false
paused=false
function timify(n)
  n=math.floor(n)
  m=math.floor(n/60)
  s=n-(60*m)
  return string.format("%02d:%02d",m,s)
end
function love.load(t)
  for i,v in ipairs(love.filesystem.getDirectoryItems("assets")) do
    if love.filesystem.isFile("assets/"..v) then
      table.insert(playlist,{["name"]=v,["sound"]=love.audio.newSource("assets/"..v,"static")})
    end
  end
  playlist[currentSong].sound:play()
  loaded=true
end
function love.update(dt)
  if not loaded then return end
  if paused then return end
  if playlist[currentSong].sound:isPlaying() then return end
  if not playlist[currentSong] then
    currentSong=1
    paused=true
    return
  end
  currentSong=currentSong+1
  playlist[currentSong].sound:play()
end
function love.draw()
  if not loaded then love.graphics.print("Loading...") end
  if paused then
    love.graphics.print(playlist[currentSong].name.." (paused)",0,0)
  else
    love.graphics.print(playlist[currentSong].name,0,0)
  end
  love.graphics.print(timify(playlist[currentSong].sound:tell()),0,10)
  love.graphics.print("Play/Pause-Space  Next/Last Song-Left/Right",0,20)
  love.graphics.print("ILoveMP3s Player by Iggyvolz - Licensed under the MIT license https://github.com/iggyvolz/ILoveMP3s",0,30)
end
function love.keypressed(k)
  if k==" " then
    if paused then
      paused=false
      playlist[currentSong].sound:play()
    else
      paused=true
      playlist[currentSong].sound:pause()
    end
  elseif k=="right" then
    playlist[currentSong].sound:stop()
  elseif k=="left" then
    if playlist[currentSong].sound:tell()<5 and currentSong ~= 1 then
      playlist[currentSong].sound:stop()
      currentSong=currentSong-1
      playlist[currentSong].sound:play()
    else
      playlist[currentSong].sound:stop()
      playlist[currentSong].sound:play()
    end
  end
end
